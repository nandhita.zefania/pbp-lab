import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FaqUtamaScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('FAQ Utama Bizzvest')),
      body: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return new StuffInTiles(listOfTiles[index]);
        },
        itemCount: listOfTiles.length,
      ),
    );
  }
}

class StuffInTiles extends StatelessWidget {
  final MyTile myTile;
  StuffInTiles(this.myTile);

  @override
  Widget build(BuildContext context) {
    return _buildTiles(myTile);
  }

  Widget _buildTiles(MyTile t) {
    if (t.children.isEmpty)
      return new ListTile(
          dense: true,
          enabled: true,
          isThreeLine: false,
          onLongPress: () => print("long press"),
          onTap: () => print("tap"),
          title: new Text(t.title, style: TextStyle(fontSize: 16.0)));

    return new ExpansionTile(
      key: new PageStorageKey<int>(3),
      backgroundColor: Colors.white70,
      collapsedBackgroundColor: Colors.white70,
      collapsedTextColor: Color(0xff374ABE),
      title: new Text(t.title, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0)),
      children: t.children.map(_buildTiles).toList(),
    );
  }
}

class MyTile {
  String title;
  List<MyTile> children;
  MyTile(this.title, [this.children = const <MyTile>[]]);
}

List<MyTile> listOfTiles = <MyTile>[
  new MyTile(
    'Bagaimana cara mendaftarkan usaha di BizzVest?',
    <MyTile>[
      new MyTile('1) Buat akun di BizzVest terlebih dahulu pada halaman sign up)'
          '\n 2) Masukkan data usaha yang anda miliki, seperti nama usaha, deskripsi usaha, jumlah dan nilai lembar saham, serta dividen dan batas waktu.'
          '\n 3) Anda dapat memasukkan foto-foto dan proposal usaha pada halaman toko agar nantinya dapat meyakinkan investor untuk berinvestasi.'
          '\n 4) Tunggu usaha Anda selesai diverifikasi oleh administrator BizzVest selama 3-5 hari kerja.'),
    ],
  ),

  new MyTile(
    'Bagaimana cara berinvestasi di BizzVest?',
    <MyTile>[
      new MyTile('1) Buat akun di BizzVest terlebih dahulu pada halaman sign up.'
          '\n 2) Pilih usaha yang ingin anda berikan investasi dengan melihat deskripsi dan proposal dari usaha tersebut.'
          '\n 3) Berikan investasi sesuai dengan nominal nilai lembar saham dan banyaknya lembar saham yang Anda inginkan.'
          '\n 4) Analisis laporan keuangan usaha sambil menantikan dividen yang akan anda terima pada batas waktu yang telah ditentukan.'),
    ],
  ),

  new MyTile(
    'Berapa lama hasil investasi atau dividen didapatkan?',
    <MyTile>[
      new MyTile('Hasil investasi pembagian keuntungan saham akan didapatkan sesuai dengan kesepakatan antara pelaku UMKM/petani dengan investor sejak awal.'),
    ],
  ),

  new MyTile(
    'Berapa lama menunggu investor memberikan modal?',
    <MyTile>[
      new MyTile('Tidak dapat dipastikan lamanya waktu untuk mendapatkan modal dari investor. BizzVest hanya menjadi perantara antara pelaku UMKM/petani dengan investor. Namun, pelaku UMKM/petani dapat memberikan video profil, proposal, atau penawaran keuntungan investasi yang menarik minat investor. Usaha tersebut diharapkan dapat memperbesar kemungkinan mendapatkan modal dari investor.'),
    ],
  ),

  new MyTile(
    'Kenapa harus bergabung dengan BizzVest?',
    <MyTile>[
      new MyTile('BizzVest hadir sebagai perantara bagi pelaku bisnis seperti pemilik UMKM (Usaha Mikro, Kecil, dan Menengah) dan petani untuk mendapatkan modal usaha dari para investor agar dapat melanjutkan bisnis mereka. Siapapun dapat bertindak sebagai penerima atau pemberi modal sesuai dengan ketentuan yang berlaku.'),
    ],
  ),

];