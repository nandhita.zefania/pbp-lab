import 'package:flutter/material.dart';

import '../widgets/main_drawer.dart';


class AboutScreen extends StatelessWidget {
  static const routeName = '/about';

  @override
  Widget build(BuildContext context) {

    final judul = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'BIZZVEST',
        style: TextStyle(fontSize: 35.0, fontWeight: FontWeight.bold, color: Colors.white),
      ),
    );

    final slogan = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        '“Let’s invest toward your business”',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold, color: Colors.white),
      ),
    );

    final lorem = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text('\nPandemi Covid-19 sudah berlangsung cukup lama dan '
          'perekonomian Indonesia telah terpengaruh oleh permasalahan ini. '
          'Banyak masyarakat Indonesia yang juga terkena dampak dari pandemi '
          'Covid-19 dan mengalami masalah keuangan padahal mereka harus terus '
          'menyambung hidup mereka. \n\nOleh karena itu, BizzVest hadir sebagai '
          'jalan keluar bagi pelaku bisnis seperti pemilik UMKM (Usaha Mikro, Kecil, '
          'dan Menengah) dan petani untuk mendapatkan modal usaha dari para investor '
          'agar dapat melanjutkan bisnis mereka. \n\nBizzVest akan menjadi intermediaries '
          'untuk mempertemukan pelaku usaha dan investor dalam melakukan investasi '
          'melalui website BizzVest. Pelaku usaha harus memberikan proposal dan video '
          'profil dari bisnis yang mereka miliki untuk dapat menarik perhatian investor. '
          '\n\nInvestor dapat mengakses daftar usaha yang terdaftar dari BizzVest dan memilih '
          'untuk berinvestasi. Setelah dilakukannya investasi, pelaku bisnis harus membuat '
          'laporan pemasukan dan pengeluaran dari usaha yang mendapatkan dana investasi tersebut, '
          'lalu melaporkannya dengan transparan dan detail agar terdapat kepercayaan antara '
          'investor dan pelaku usaha. \n\nKemudian, akan dilakukan bagi hasil antara pelaku bisnis '
          'dan investor sesuai dengan keuntungan yang didapatkan dan kesepakatan sebelumnya.',
        textAlign: TextAlign.justify,
        style: TextStyle(fontSize: 20.0, color: Colors.white),
      ),
    );

    final body = Container(
      padding: EdgeInsets.all(28.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Colors.lightBlue,
          Colors.lightBlueAccent,
        ]),
      ),
      child: Column(
        children: <Widget>[judul, slogan, lorem],
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('About Bizzvest'),
      ),
      drawer: MainDrawer(),
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(child: body)
    );
  }
}
