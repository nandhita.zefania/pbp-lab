import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/screens/faqlist_screen.dart';

class FormScreen extends StatelessWidget {
  static const routeName = '/form';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(child: Column(
        children: <Widget>[
          Center(
              child: Padding(
                padding: EdgeInsets.all(35.0),
                  child: Text(
                    "Tanyakan pertanyaanmu!",
                    style: TextStyle(
                      fontSize: 25.0,
                      color: Color(0xff374ABE),
                      fontWeight: FontWeight.bold,
                    ),
                  )
            )
          ),

          new Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsetsDirectional.only(top: 25.0, bottom: 5.0, start: 16.0, end: 16.0),
              child: Text(
                "Nama:",
                softWrap: true,
                style: new TextStyle(
                    fontSize: 17.0,
                    fontWeight: FontWeight.bold,
                    color: Color(0xff374ABE)),
              ),
            ),
          ),

          Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                children: <Widget>[
                  TextField(
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white70,
                      hintText: "Tuliskan nama Anda...",
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(width: 1, color: Colors.blue),
                        borderRadius: BorderRadius.circular(15),
                      ),
                    ),
                  )
                ],
              ),

            ),
          ),

          new Align(
            alignment: Alignment.centerLeft,
            child: Padding(
            padding: const EdgeInsetsDirectional.only(top: 25.0, bottom: 5.0, start: 16.0, end: 16.0),
            child: Text(
              "Pertanyaan:",
              softWrap: true,
              style: new TextStyle(
                  fontSize: 17.0,
                  fontWeight: FontWeight.bold,
                  color: Color(0xff374ABE)),
              ),
            ),
          ),

          Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                children: <Widget>[
                  TextField(
                    maxLines: 7,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white70,
                      hintText: "Tuliskan pertanyaan Anda...",
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(width: 1, color: Colors.blue),
                        borderRadius: BorderRadius.circular(15),
                      ),
                    ),
                  )
                ],
              ),

            ),
          ),

          SizedBox(height: 20.0),
          MaterialButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            padding: EdgeInsets.all(10.0),
            color: Color(0xff374ABE),
            onPressed: () {
              _navigateToNextScreen(context);
            },
            child: Text("Kirimkan pertanyaan", style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.white70,
              fontSize: 17.0
            ))
          )

        ],
      )
    ));
  }
  void _navigateToNextScreen(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => FaqListScreen()));
  }
}
