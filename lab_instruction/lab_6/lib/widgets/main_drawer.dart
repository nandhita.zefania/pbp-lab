import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/screens/about_screen.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      hoverColor: Colors.blueAccent,
      tileColor: Colors.lightBlueAccent,
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 180,
            width: double.infinity,
            padding: EdgeInsets.all(40),
            alignment: Alignment.centerLeft,
            color: Theme.of(context).backgroundColor,
            child: Center( child: Text(
              'FAQ BIZZVEST!',
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Theme.of(context).accentColor),
            ),
          ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Home', Icons.home_rounded, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          SizedBox(
            height: 10,
          ),
          buildListTile('About', Icons.person_rounded, () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => new AboutScreen()),
            );
          }),
        ],
      ),
    );
  }
}
