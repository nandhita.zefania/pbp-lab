import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FaqListScreen extends StatelessWidget {

  final List pertanyaan = [
    "Bagaimana membuat proposal yang baik?",
    "Bagaimana cara mendapatkan badges investor?",
    "Apakah Bizzvest dapat dipercaya?",
  ];

  final List penanya = [
    "Arianna Mawar",
    "Langitnya Nebula",
    "Arani Dhita",
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('FAQ Pengguna Bizzvest')),
      body: ListView.builder(
        itemBuilder: (context, index) {
          return Card(
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                children: <Widget> [
                  Text(pertanyaan[index], style: TextStyle(fontSize: 17, color: Color(0xff374ABE), fontWeight: FontWeight.bold)),
                  Text("Pertanyaan dari " + penanya[index], style: TextStyle(fontSize: 17, color: Color(0xff374ABE))),
                ],
              )
            ),
          );
        },
        itemCount: pertanyaan.length,
      ),
    );
  }
}