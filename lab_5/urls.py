from django.urls import path
from lab_5.views import index, update_note, delete_note, get_note


urlpatterns = [
    path('', index, name='index'),
    path('notes/<id>/update', update_note, name="update_notes"),
    path('notes/<id>', get_note, name="view_notes"),
    path('notes/<id>/delete', delete_note, name="delete_notes"),
]