from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from .forms import NoteForm
from lab_2.models import Note

# Create your views here.
def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab5_index.html', response)

def get_note(request, id):
    context = {}

    context["data"] = Note.objects.get(id=id)

    return render(request, "get_view.html", context)

def update_note(request, id):
    context = {}

    obj = get_object_or_404(Note, id=id)

    form = NoteForm(request.POST or None, instance=obj)

    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/" + id)

    context["form"] = form

    return render(request, "update_view.html", context)

def delete_note(request, id):
    context = {}

    obj = get_object_or_404(Note, id=id)

    if request.method == "POST":

        obj.delete()

        return HttpResponseRedirect("/")

    return render(request, "delete_view.html", context)