Nama    : Nandhita Zefania Maharani
NPM     : 2006463963
Kelas   : C


Apakah perbedaan antara JSON dan XML?
Jawab:
1. Ukuran file
JSON memiliki file yang lebih ringan dibandingkan dengan XML karena kode yang digunakan oleh JSON lebih sederhana.
   
2. Pengiriman data
JSON yang memiliki kode lebih sederhana membuat waktu pengiriman datanya akan lebih cepat dibandingkan dengan XML.

3. Penyimpanan data
JSON disimpan dengan key dan value seperti suatu map, sedangkan XML disimpan sebagai tree structure.
   
4. Tipe data yang didukung
JSON hanya mendukung objek dengan tipe data yang primitif, seperti string, integer, boolean, dan lain sebagainya. 
Sedangkan, XML dapat mendukung tipe data kompleks seperti charts, bagan, dan tipe data non primitif lainnya.
   
5. Basis bahasa
JSON merupakan Object JavaScript, sedangkan XML berasal dari SGML (Standard Generalized Markup Language).
   
6. Pengolahan data
JSON tidak melakukan proses perhitungan apapun, sedangkan XML dapat melakukan proses pemformatan dokumen dan objek.
Selain itu, JSON juga dapat melakukan proses pengolahan data yang lebih cepat karena diproses secara serial.
Sedangkan, XML akan melakukan proses pengolahan data dengan lebih lambat karena tag di XML mengonsumsi lebih banyak bandwith.
   
7. Tampilan sintaks dan namespace
JSON tidak dapat diubah tampilan sintaksnya karena tidak menggunakan tag, sedangkan XML dapat diubah karena merupakan markup language.
Selain itu, JSON tidak mendukung namespace sedangkan XML mendukung namespace.

8. Dukungan UTF
JSON hanya mendukung penyandian aksara UTF dan ASCII, sedangkan XML mendukung pengkodean UTF-8 dan UTF-16
   
9. Kasus terbaik untuk dipakai
JSON lebih baik digunakan untuk mengolah data besar ataupun pertukaran data, seperti web based integration
Sedangkan, XML lebih baik digunakan ketika ingin membuat aplikasi cross-platform.


Apakah perbedaan antara HTML dan XML?
Jawab:
1. Fokus utama
HTML atau yang biasa disebut dengan Hypertext Markup Language memiliki fokus utama terhadap penyajian data.
Sedangkan, XML atau yang biasa disebut dengan Extensible Markup Language memiliki fokus utama terhadap transfer data
   
2. Bahasa
HTML case insensitive sedangkan XML case sensitive terhadap pemakaian huruf besar dan huruf kecil.
   
3. Tag penutup dan spasi
HTML tidak dapat mempertahankan spasi dan tag penutup merupakan optional untuk diberikan.
Sedangkan, XML dapat mempertahankan spasi dan harus menggunakan tag penutup.

4. Pertukaran data
HTML tidak memungkinkan untuk terjadi pertukaran data, sedangkan XML dapat melakukan pertukaran data
   
5. Nilai pada atribut
Atribut pada HTML dapat hadir tanpa nilai dan tanpa tanda kutip, sedangkan XML harus memiliki nilai jika didefinisikan dan harus diapit tanda kutip.
   
6. Tampilan
HTML adalah tentang menampilkan data dan bersifat statis. Sedangkan, XML adalah tentang menggambarkan data dan bersifat dinamis.
   
7. Kesalahan kode
Kesalahan kecil pada HTML dalam pembuatan source code dapat diabaikan dan hasilnya tetap dapat dicapai.
Sedangkan, kesalahan kecil pada XML pada pembuatan source code tidak akan memberikan hasil akhir.

8. Ukuran file
Ukuran file HTML lebih kecil dibandingkan XML karena pada HTML tidak ada dokumen yang panjang (hanya perlu menambahkan sintaks agar mendapatkan output dengan format terbaik)


Referensi:
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://www.geeksforgeeks.org/difference-between-json-and-xml/
https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html
https://id.natapa.org/difference-between-xml-and-html-2446
https://byjus.com/free-ias-prep/difference-between-xml-and-html/