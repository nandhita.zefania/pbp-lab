from django.db import models

# Create your models here.
class Note(models.Model):
    penerima = models.CharField(max_length=30)
    pengirim = models.CharField(max_length=30)
    judul = models.CharField(max_length=30)
    pesan = models.CharField(max_length=30)